
var fieldSets = new Array();
var fieldSet;
var airports;
var airportAuto;

String.prototype.toTitleCase = function(n) {
    var s = this;
    if (1 !== n) s = s.toLowerCase();
    return s.replace(/(^|\s)[a-z]/g,function(f){return f.toUpperCase()});
}

function supports_html5_storage() {
  try {
    return 'localStorage' in window && window['localStorage'] !== null;
  } catch (e) {
    return false;
  }
}

Storage.prototype.setObject = function(key, value) {
    this.setItem(key, JSON.stringify(value));
    var output = this.getItem(key);
    debugger;
}

Storage.prototype.getObject = function(key) {
    var value = this.getItem(key);
    return value && JSON.parse(value);
}

function writeCookies(flightNos) {
  //for (var it in $.cookie()) $.removeCookie(it);
  $.cookie.json = true;
  $.cookie("flightNos", flightNos, {path: '/' });
}

window.addEventListener('load', eventWindowLoaded, false);

function eventWindowLoaded() {
  //  canvasApp();
  var flighthistoryform = document.getElementById('flighthistoryform');
  fieldSet = flighthistoryform.getElementsByTagName("fieldset")[0];
  document.getElementById("minus").style.display = 'none';
  //parse Airport CSV
  var csv;
  var flights;

  var getCSV = new XMLHttpRequest();

  getCSV.open('GET', "/static/Airport7.csv", true); // Path to File
  //getCSV.responseType = 'arraybuffer'; // Read as Binary Data
  getCSV.onreadystatechange = function() {
    if (getCSV.readyState == XMLHttpRequest.DONE) {
          csv = getCSV.responseText;
          var dataObj;
          //console.log("CSV = " + csv);
          Papa.parse(csv, {
               header: true,
            delimiter:":",
        dynamicTyping: true,
             complete: function(results) {
                  //console.log("results " + results);
                  dataObj = results;
             }
          });

          //console.log(dataObj); //.data[0].airport);
          console.log(dataObj.data[0]);
          console.log(dataObj.error);
          //console.log(dataObj.data[0]);
          console.log("Airport[0] = " + dataObj.data[0].airport);
          airports = dataObj.data;

          //make clean Airport list
          airportAuto = new Array();

          for (var i = 0; i < airports.length - 1; i++) {
            airportAuto.push(airports[i].prefix + airports[i].city + airports[i].suffix);
          }

          for (var i = 0; i < airportAuto.length; i++) {
            for (var j = i + 1; j < airportAuto.length; j++) {
              if (airportAuto[i] == airportAuto[j]) {
                console.log("duplicate: " + airportAuto[i]);
                break;
              }
            }
          }
          console.log(airportAuto.length);
          $( ".autocomplete" ).autocomplete({
               minLength: 3,
                  source: airportAuto,
               autoFocus:true
          });

          addFieldSet();

          //console.log(dataObj); //.data[0].airport);
          console.log(dataObj.data[0]);
          console.log(dataObj.error);
          //console.log(dataObj.data[0]);
          console.log("Airport[0] = " + dataObj.data[0].airport);
          airports = dataObj.data;
          var flightNos = getCookies();
          console.log(location);
          console.log(location.pathname);
          if (flightNos) {
            flights = processFlights(flightNos);
            if (flights) {
              t0 = Date.now();
            	if (location.pathname.localeCompare("/fr/output.html") == 0) {
	               anim(flights);
            	}
            }
            else {
            	if (location.pathname.localeCompare("/fr/output.html") == 0) {
            	   location = "/";
            	}
            }
          }
          else {
         	if (location.pathname.localeCompare("/fr/output.html") == 0) {
         	   location = "/";
         	}
          }
    };
	}
  getCSV.send(); // Send the Request and Load the File

  $(".nav-tabs a").click(function(){
    $(this).tab('show');
  });

  switch(location.hash) {
    case "#flighthistory":
      $('.nav-tabs a[href="#flighthistory"]').tab('show');
      //do nothing
      break;
    case "#singleflight":
      $('.nav-tabs a[href="#singleflight"]').tab('show');
      break;
    case "#about":
      $('.nav-tabs a[href="#about"]').tab('show');
      break;
  }
}//Form
function removeFieldSet() {
  var removal = fieldSets.pop();
  var lastLine = removal.parentNode;
  lastLine.parentNode.removeChild(lastLine);
  if (fieldSets.length <= 1) {
    document.getElementById("minus").style.display = 'none';
  }
}

function addFieldSet() {
  //get flight data from form
  var flighthistoryform = document.getElementById('flighthistoryform');
  var newFieldSet = fieldSet.cloneNode(true);
  fieldSets.push(newFieldSet);
  var buttons = document.getElementById('buttons');
  var newParagraph = document.createElement('p');
  newParagraph.appendChild(newFieldSet);
  flighthistoryform.insertBefore(newParagraph, buttons);
  if (fieldSets.length > 1) {

    document.getElementById("minus").style.display = 'inline';
  }

  var count = (fieldSets.length - 1);
  newFieldSet.id = "FieldSet" + count;
  $( '#' + newFieldSet.id + " input[name='year']" ).attr("id","YearInput" + count);
  $( '#' + newFieldSet.id + " input[name='origin']" ).attr("id","OriginInput" + count);
  $( '#' + newFieldSet.id + " input[name='destination']" ).attr("id","DestinationInput" + count);
  $( '#' + newFieldSet.id + " select[name='return']" ).attr("id","ReturnSelect" + count);
  $( '#' + newFieldSet.id + " a[name='duplicate']" ).attr("id","DuplicateLink" + count);

  $( '#DuplicateLink' + count).click( function(e) {e.preventDefault();
                                                    duplicateFieldSet(count);
                                                    return false; } );

  setupInputs();
}

function duplicateFieldSet(value) {
  var flighthistoryform = document.getElementById('flighthistoryform');
  var oldFieldSet = document.getElementById('FieldSet' + value)
  var newFieldSet = oldFieldSet.cloneNode(true);
  fieldSets.push(newFieldSet);
  var buttons = document.getElementById('buttons');
  var newParagraph = document.createElement('p');
  newParagraph.appendChild(newFieldSet);
  flighthistoryform.insertBefore(newParagraph, buttons);
  if (fieldSets.length > 1) {

    document.getElementById("minus").style.display = 'inline';
  }

  var count = (fieldSets.length - 1);
  newFieldSet.id = "FieldSet" + count;
  console.log(newFieldSet.id);

  $( '#' + newFieldSet.id + " input[name='year']" ).attr("id","YearInput" + count);
  $( '#' + newFieldSet.id + " input[name='origin']" ).attr("id","OriginInput" + count);
  $( '#' + newFieldSet.id + " input[name='destination']" ).attr("id","DestinationInput" + count);
  $( '#' + newFieldSet.id + " select[name='return']" ).attr("id","ReturnSelect" + count);
  $( '#' + newFieldSet.id + " a[name='duplicate']" ).attr("id","DuplicateLink" + count);

  $( '#DuplicateLink' + count).click( function(e) {e.preventDefault();
                                                    duplicateFieldSet(count);
                                                    return false; } );

  var returnValue = $( "#ReturnSelect" + value + " option:selected" ).text();
  $("#ReturnSelect" + count + " option").filter(function() {
          return $(this).text() == returnValue;
        }).prop('selected', true);
  setupInputs();
}

function setupInputs() {
  $( ".autocomplete" ).autocomplete({
       minLength: 3,
          source: airportAuto,
       autoFocus:true,
           close: function( event, ui ) {
        var elem = $( this );
        var input = elem[0].value;
        console.log(elem);
        var check = false;
        for (var i = 0; i < airportAuto.length; i++) {
          if (input.localeCompare(airportAuto[i]) == 0) {
            elem.parent().find("span").addClass("glyphicon-ok");
            elem.parent().find("span").removeClass("glyphicon-warning-sign glyphicon-remove");
            elem.parent().addClass("has-success");
            elem.parent().removeClass("has-warning has-error");
            elem.css("background-color", "#D6FFD6");
            check = true;
            break;
          }
        }
        if (!check) {
          elem.parent().find("span").addClass("glyphicon-remove");
          elem.parent().find("span").removeClass("glyphicon-warning-sign glyphicon-ok");
          elem.parent().addClass("has-error");
          elem.parent().removeClass("has-warning has-success");
          elem.css("background-color", "#FFD6D6");
        }
      }
  });

  $( ".autocomplete" ).autocomplete( "option", "classes.ui-autocomplete", "fixed-height" );

  $("input[name='year']").on(
    "change, focusin, focusout, keydown, keyup", function(eventObject) {
      var elem = $( this );
      var date = new Date();
      var year = date.getFullYear();
      var input = parseInt(elem[0].value);
      if (elem[0].value === "") {
        elem.parent().find("span").removeClass("glyphicon-warning-sign glyphicon-remove glyphicon-ok");
        elem.parent().removeClass("has-warning has-error has-success");
        elem.css("background-color", "#FFFFFF");
      }
      else if (input == Math.floor(input) && input >= 0 && input <= year) {
        if (input >= 1903) {
          elem.parent().find("span").addClass("glyphicon-ok");
          elem.parent().find("span").removeClass("glyphicon-remove glyphicon-warning-sign");
          elem.parent().addClass("has-success");
          elem.parent().removeClass("has-error has-warning");
          elem.css("background-color", "#D6FFD6");
        }
        else {
          elem.parent().find("span").addClass("glyphicon-warning-sign");
          elem.parent().find("span").removeClass("glyphicon-ok glyphicon-remove");
          elem.parent().addClass("has-warning");
          elem.parent().removeClass("has-success has-error");
          elem.css("background-color", "#FFF0B6");
        }
      }
      else {
        elem.parent().find("span").addClass("glyphicon-remove")
        elem.parent().find("span").removeClass("glyphicon-ok glyphicon-warning-sign");
        elem.parent().addClass("has-error");
        elem.parent().removeClass("has-success has-warning");
        elem.css("background-color", "#FFD6D6");
        console.log(elem );
      }
  }
  );
  $( "input[name='origin'], input[name='destination']" ).on(
    "change",
    function( eventObject ) {
        var elem = $( this );
        var input = elem[0].value;
        console.log(elem);
        var check = false;
        if (input === "") {
          elem.parent().find("span").removeClass("glyphicon-warning-sign glyphicon-remove glyphicon-ok");
          elem.parent().removeClass("has-warning has-error has-success");
          elem.css("background-color", "#FFFFFF");
          check = true;
        }
        else {
          for (var i = 0; i < airportAuto.length; i++) {
            if (input.localeCompare(airportAuto[i]) == 0) {
              elem.parent().find("span").addClass("glyphicon-ok");
              elem.parent().find("span").removeClass("glyphicon-warning-sign glyphicon-remove");
              elem.parent().addClass("has-success");
              elem.parent().removeClass("has-warning has-error");
              elem.css("background-color", "#D6FFD6");
              check = true;
              break;
            }
          }
        }
        if (!check) {
          elem.parent().find("span").addClass("glyphicon-remove");
          elem.parent().find("span").removeClass("glyphicon-warning-sign glyphicon-ok");
          elem.parent().addClass("has-error");
          elem.parent().removeClass("has-warning has-success");
          elem.css("background-color", "#FFD6D6");
        }
    }
  );

  $( "input[name='origin'], input[name='destination']" ).on(
    "keydown, keyup, focusin",
    function ( eventObject ) {
      var elem = $( this );
      var input = elem[0].value;
      var pattern = new RegExp(input, "i");
      var check = false;
      for (var i = 0; i < airportAuto.length; i++) {
        if (pattern.test(airportAuto[i])) {
          if (input.localeCompare(airportAuto[i]) == 0) {
            elem.parent().find("span").addClass("glyphicon-ok");
            elem.parent().find("span").removeClass("glyphicon-warning-sign glyphicon-remove");
            elem.parent().addClass("has-success");
            elem.parent().removeClass("has-warning has-error");
            elem.css("background-color", "#D6FFD6");
          }
          else {
            elem.parent().find("span").addClass("glyphicon-warning-sign");
            elem.parent().find("span").removeClass("glyphicon-ok glyphicon-remove");
            elem.parent().addClass("has-warning");
            elem.parent().removeClass("has-success has-error");
            elem.css("background-color", "#FFF0B6");
          }
          check = true;
          break;
        }
      }
      if (!check) {
        elem.parent().find("span").addClass("glyphicon-remove");
        elem.parent().find("span").removeClass("glyphicon-warning-sign glyphicon-ok");
        elem.parent().addClass("has-error");
        elem.parent().removeClass("has-warning has-success");
        elem.css("background-color", "#FFD6D6");
      }
    }
  );
  $( "input[name='origin'], input[name='destination']" ).on(
    "focusout",
    function ( eventObject ) {
      var elem = $( this );
      var input = elem[0].value;
      console.log(elem);
      var check = false;
      if (input === "") {
        elem.parent().find("span").removeClass("glyphicon-warning-sign glyphicon-remove glyphicon-ok");
        elem.parent().removeClass("has-warning has-error has-success");
        elem.css("background-color", "#FFFFFF");
        check = true;
      }
      else {
        for (var i = 0; i < airportAuto.length; i++) {
          if (input.localeCompare(airportAuto[i]) == 0) {
            elem.parent().find("span").addClass("glyphicon-ok");
            elem.parent().find("span").removeClass("glyphicon-warning-sign glyphicon-remove");
            elem.parent().addClass("has-success");
            elem.parent().removeClass("has-warning has-error");
            elem.css("background-color", "#D6FFD6");
            check = true;
            break;
          }
        }
        if (!check) {
          elem.parent().find("span").addClass("glyphicon-remove");
          elem.parent().find("span").removeClass("glyphicon-warning-sign glyphicon-ok");
          elem.parent().addClass("has-error");
          elem.parent().removeClass("has-warning has-success");
          elem.css("background-color", "#FFD6D6");
        }
      }
    }
  );

  $( "select[name='return']" ).on(
    "change, focusin, focusout, keydown, keyup, click",
    function( eventObject ) {
        var elem = $( this );
        var input = elem[0].value;
        if (input.localeCompare("Return") == 0 || input.localeCompare("Single")) {
            elem.parent().find("span").addClass("glyphicon-ok");
            elem.parent().find("span").removeClass("glyphicon-warning-sign glyphicon-remove");
            elem.parent().addClass("has-success");
            elem.parent().removeClass("has-warning has-error");
            elem.css("background-color", "#D6FFD6");
        }
        else {
          elem.parent().find("span").addClass("glyphicon-remove");
          elem.parent().find("span").removeClass("glyphicon-warning-sign glyphicon-ok");
          elem.parent().addClass("has-error");
          elem.parent().removeClass("has-warning has-success");
          elem.css("background-color", "#FFD6D6");
        }
      }
  );


    $( "select[name='return']" ).focusout(function( eventObject ) {
          var elem = $( this );
          var input = elem[0].value;
          if (input.localeCompare("Return") == 0 || input.localeCompare("Single")) {
              elem.parent().find("span").addClass("glyphicon-ok");
              elem.parent().find("span").removeClass("glyphicon-warning-sign glyphicon-remove");
              elem.parent().addClass("has-success");
              elem.parent().removeClass("has-warning has-error");
              elem.css("background-color", "#D6FFD6");
          }
          else {
            elem.parent().find("span").addClass("glyphicon-remove");
            elem.parent().find("span").removeClass("glyphicon-warning-sign glyphicon-ok");
            elem.parent().addClass("has-error");
            elem.parent().removeClass("has-warning has-success");
            elem.css("background-color", "#FFD6D6");
          }
        }
    );
}

function processSingleFlight(theForm) {
  var flights = new Array();
  var flightNos = new Array();
  var formValid = true;
  var oFieldSet = $("#singlefieldset");
  var elem = oFieldSet.find("input[name='origin']");
  origin = elem[0].value;
  elem = oFieldSet.find("input[name='destination']");
  destination = elem[0].value;
  elem = oFieldSet.find("select[name='return']");
  if (elem[0].value === "return") {
    returnBool = true;
  }
  else {
    returnBool = false;
  }

  var flight = {year:"",
              origin:origin,
         destination:destination,
              return:returnBool};

  var flightNo = {y:"",
                  o:0,
                  d:0,
                  r:returnBool};

  var check = false;
  for (var j = 0; j < airports.length; j++) {
    if (flight.origin == airportAuto[j]) {
      flightNo.o = j;
      check = true;
      break;
    }
  }
  if (!check) {
    formValid = false;
  }

  check = false;
  for (var j = 0; j < airports.length; j++) {
    if (flight.destination == airportAuto[j]) {
      flightNo.d = j;
      check = true;
      break;
    }
  }
  if (!check) {
    formValid = false;
  }

  flightNos.push(flightNo);

  if (formValid) {
      writeCookies(flightNos);
      return true;
  }
  else {
    document.getElementById('sferrormessage').style.display = 'block';
    return false;
  }
}

function processFlightHistory(theForm) {
  var flightNos = new Array();
  var formValid = true, check = false;
  var date = new Date();
  var year = date.getFullYear();
  for (var i = 0; i < fieldSets.length; i++) {
    var tFieldSet = fieldSets[i];
    var children = tFieldSet.childNodes;
    var flightyear, origin, destination, returnBool;
    for (var j = 0; j < children.length; j++) {
      var child = children[j];
      var grandchildren = child.childNodes;
      for (var k = 0; k < grandchildren.length; k++) {
        var element = grandchildren[k];
      switch (element.name) {
        case "year":
          flightYear = parseInt(element.value);
          break;
        case "origin":
          origin = element.value;
          break;
        case "destination":
          destination = element.value;
          break;
        case "return":
          if (element.value == "return") {
            returnBool = true;
          }
          else {
            returnBool = false;
          }
          break;
        }
      }
    }
    if (flightYear < 1903 || flightYear > year) {
      formValid = false;
    }

      var flight = {year:flightYear,
                  origin:origin,
             destination:destination,
                  return:returnBool};

      var flightNo = {y:flightYear,
                      o:0,
                      d:0,
                      r:returnBool};

      var check = false;
      for (var j = 0; j < airports.length; j++) {
        if (flight.origin == airportAuto[j]) {
          flightNo.o = j;
          check = true;
          break;
        }
      }
      if (!check) {
        formValid = false;
        break;
      }

      check = false;
      for (var j = 0; j < airports.length; j++) {
        if (flight.destination == airportAuto[j]) {
          flightNo.d = j;
          check = true;
          break;
        }
      }
      if (!check) {
        formValid = false;
        break;
      }

      flightNos.push(flightNo);
  }
  if (formValid) {
      writeCookies(flightNos);
      return true;
  }
  else {
    document.getElementById('errormessage').style.display = 'block';
    return false;
  }
}
const STATE_CALC = 0;
const STATE_SHARE = 10;
const STATE_STILL = 20;
var appState = STATE_CALC;
var t0, t1;

function getCookies() {
  $.cookie.json = true;
  var flightNos = $.cookie('flightNos');
  return flightNos;
}

function distance(o, d) {
  var R = 6371e3; // radius of the earth in metres
  var phi1 = Math.PI * o.lat / 180;
  var phi2 = Math.PI * d.lat / 180;
  var deltaphi = Math.PI * (d.lat - o.lat) / 180;
  var deltalamda = Math.PI * (d.long - o.long) / 180;

  var a = Math.sin(deltaphi / 2) * Math.sin(deltaphi / 2) + Math.cos(phi1) * Math.cos(phi2) * Math.sin(deltalamda / 2) * Math.sin(deltalamda / 2);
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
  var distance = R * c;

  return distance;
}

function intermediatePoint(f, o, d) {
  var delta = distance(o, d) / 6371e3;
  var a = Math.sin((1 - f) * delta) / Math.sin(delta);
  var b = Math.sin(f * delta) / Math.sin(delta);

  var x = a * Math.cos(Math.PI * o.lat / 180) * Math.cos(Math.PI * o.long / 180) + b * Math.cos(Math.PI * d.lat / 180) * Math.cos(Math.PI * d.long / 180);
  var y = a * Math.cos(Math.PI * o.lat / 180) * Math.sin(Math.PI * o.long / 180) + b * Math.cos(Math.PI * d.lat / 180) * Math.sin(Math.PI * d.long / 180);
  var z = a * Math.sin(Math.PI * o.lat / 180) + b * Math.sin(Math.PI * d.lat / 180);

  return {long:Math.atan2(y, x) * 180 / Math.PI, lat:Math.atan2(z, Math.sqrt((x * x) + (y * y))) * 180 / Math.PI};
}

function processFlights(flightNos) {
  var flights = new Array();
  var cookieOK = true, check = false;
  for (var i = 0; i < flightNos.length; i++) {
    var f = flightNos[i];

    var flight = {year:f.y,
                flight:true,
                origin:"",
                     o:{long:0, lat:0},
           destination:"",
                     d:{long:0, lat:0},
              distance:0,
                  path:[],
                return:f.r};
    flights.push(flight);

    var o = {long:-56, lat:-22};

    if ((f.o >= 0) && (f.o < airports.length)) {
      flight.origin = airports[f.o].city;
      o.long = airports[f.o].longitude;
      o.lat = airports[f.o].latitude;
      check = true;
    }
    if (!check) {
      cookieOK = false;
    }
    flight.o = o;

    check = false;
    var d = {long:80, lat:60};

    if ((f.d >= 0) && (f.d < airports.length) && (f.d != f.o)) {
      flight.destination = airports[f.d].city;
      d.long = airports[f.d].longitude;
      d.lat = airports[f.d].latitude;
      check = true;
    }
    if (!check) {
      cookieOK = false;
    }
    flight.d = d;

    flight.distance = distance(flight.o, flight.d);
  }

  flights.sort(function(a, b) {
    return a.year - b.year;
  });

  var startYear = flights[0].year;
  var endYear = flights[flights.length-1].year;
  var flightNo = 0;
  for (var i = startYear; i <= endYear; i++) {
    var done = false;
    while (!done) {
      if (flights[flightNo].year == i) {
        flightNo++;
        done = true;
      }
      else if (flights[flightNo].year > i) {
        flights.splice(flightNo, 0, {year:i, flight:false, origin:'null', o:{long:0, lat:0}, destination:'null', d:{long:0, lat:0}, distance:0, path:[], return:false});
        flightNo++;
        done = true;
      }
      else {
        flightNo++;
      }
    }
  }
  if (cookieOK) {
    return flights;
  }
  else {
    return false;
  }
}

function anim(flights) {
   var output = "";
	var out = "";
   var link = "", email = "", phrase = "", twitter = "", facebook = "", diaspora = "";
   var share = "Partagez :";
   
   function carbon(d) {
   	if (d < 1000) {
   		return d * 0.00016236;	//CO2e = 162.36gCO2/km
   	}
   	else if (d < 2500) {
   		return d * 0.00021256;  //CO2e = 212.56gCO2/km
   	}
   	else {
   		return d * 0.00018277;   //CO2e = 182.77gCO2/km
   	}
   }
   
   function convertToHTML(unicodeText) {
      var newText = "";
		for (var i = 0; i < unicodeText.length; i++) {
		   console.log(unicodeText.charAt(i));
			if (unicodeText.charAt(i).localeCompare(" ") == 0) {
				newText += "%20";
			}
			else {
				newText += unicodeText.charAt(i);
			}
		}
		return newText;
   }
   
   function generateOutput() {
   var emissions = 0;
   var totDist = 0;
     for (var i = 0; i < flights.length; i++) {
        if (flights[i].flight) {
	        out += "Vol de : " + flights[i].origin + " à : " + flights[i].destination;
	        if (flights[i].return) {
	           out += " aller retour";
	        }
	        out += "\n";
		     var d = distance(flights[i].o, flights[i].d);
		     var e = carbon(d/1000);
		     if (flights[i].return) {
		     	  d *= 2;
		     	  e *= 2;
		     }
		     out += "Distance : " + Math.round(d/1000) + " km   ";
		     out += "<b>CO2e : " + Math.round(10 * e) / 10 + " tonnes par passager</b>\n\n";
		     totDist += d;
		     emissions += e;
        }
     }
     if (flights.length > 1) {
	     out += "<i>Dans votre vie :</i>\n";
	     out += "Distance : " + Math.round(totDist/1000) + " km   ";
	     out += "<b>CO2e : " + Math.round(10 * emissions) / 10 + " tonnes par passager</b>\n\n";
     }
     
     output += "<i>C'est comme :</i>\n";
     var decades = Math.round(emissions / 0.000010274 / 24 / 365) / 10;
     if (decades > 1.6) {
     	  output += "* Laisser allumé <i>une ampoule basse consomation</i> toute la journée et toute la nuit <i>pendant " + Math.round(decades) + " décennies</i>.\n";
     }
     else if (decades > 0.7) {
        output += "* Laisser allumé <i>une ampoule basse consomation</i> toute la journée et toute la nuit <i>pendant une décennie</i>.\n";
     }
     var circum = Math.floor(emissions / 0.000011 / 40000);
     if (circum > 0) {
        if (circum == 1) {
           output += "* Voyager l'équivalent d'<i>un fois autour du monde en train</i>.\n";
        }
        else {
        	  output += "* Voyager l'équivalent de <i>" + circum + " fois autour du monde en train</i>.\n";
        }
     }
     output += "* Prendre <i>une douche de 6 minutes un fois par jour pendant " + Math.round(emissions / 0.00055 / 365) + " ans</i>.\n";
     if (decades <= 0.7) {
        output += "* Laisser allumé <i>une ampoule basse consomation</i> toute la journée et toute la nuit <i>pendant " + Math.round(emissions / 0.000010274 / 24 / 365) + " ans</i>.\n"
     }
     output += "* Manger <i>" + Math.round(emissions / 0.0025) + " cheeseburgers</i> ou <i>" + Math.round(emissions / 0.001) + " burgers végétariens</i>.\n"
     if (circum == 0) {
        output += "* Voyager <i>" + Math.round(emissions / 0.000011) + " km en train</i>.\n";	
     }
     output += "* Envoyer <i>" + Math.round(emissions / 0.000004) + " mails</i>.";
  
     email += "<a href='mailto:?subject=Serieusement%20ludique%20%3A%20le%20Convertisseur%20Flight2fart";
     email += "&amp;body=Psitt%21%0A%0A";
     if (flights.length == 1) {
	     phrase += "Saviez%20vous%20que%2E%2E%2E%20un%20vol%20";
	     if (flights[0].return) {
	        phrase += "aller retour%20";
	     }
	     phrase += "de%20" + convertToHTML(flights[0].origin) + "%20à%20";
	     phrase += convertToHTML(flights[0].destination) + "%20produit%20l%27équivalent%20";
		  var e = carbon(distance(flights[0].o, flights[0].d)/1000);
		  if (flights[0].return) {
		     e *= 2;
		  }
		  phrase += "de%20" + Math.round(10 * e) / 10 + "%20tonnes%20de%20CO2%20par%20passager%2C";
	     phrase += "%20c%27est%20comme%20laisser%20allumé%20une%20ampoule%20basse%20consomation%20en%20continu%20pendant%20";
	     phrase += Math.round(e / 0.000010274 / 24 / 365) + "%20ans%21%0A%0A";
	     phrase += "Découvrez%20combien%20vos%20vols%20puent%20à%20http%3A%2F%2Fflight2fart%2Ecom%2Ffr%2F";
	     //%0A'>";
     }
     else {
	     phrase += "Saviez%20vous%20que%2E%2E%2E%20J%27ai%20produit%20dans%20ma%20vie%20";
	     phrase += "l%27équivalent%20";
		  var e = emissions;
		  phrase += "de%20" + Math.round(10 * e) / 10 + "%20tonnes%20de%20CO2%20des%20vols%20en%20avion%2C";
	     phrase += "%20c%27est%20comme%20laisser%20allumé%20une%20ampoule%20basse%20consomation%20en%20continu%20pendant%20";
	     phrase += Math.round(emissions / 0.000010274 / 24 / 365) + "%20ans%21%0A%0A";
	     phrase += "Découvrez%20combien%20vos%20vols%20puent%20à%20http%3A%2F%2Fflight2fart%2Ecom%2Ffr%2F";
	     //%0A'>";
     }
     email += phrase + "%0A'>";
     twitter = "<a href='http://twitter.com/intent/tweet?text=Psitt%21%0A%0A" + phrase + "%20%23flight2fart'>";
     facebook = "<a href='http://www.facebook.com/sharer.php?u=http://flight2fart.com/fr/'>";
     diaspora = '<a href="javascript:;" onclick="window.open(\'http://sharetodiaspora.github.io/?url=\'';
     diaspora += '+encodeURIComponent(\'http://flight2fart.com/fr/\')+\'&title=\'+encodeURIComponent(document.title),\'das\'';
     diaspora += ',\'location=yes,links=no,scrollbars=no,toolbar=yes,width=620,height=550\'); return false;" rel="nofollow" target="_blank">'
     link = "<a href='javascript:;' id='share'>"
   }
   
	function smoothAlpha(alpha) {
	  return 1 - ((1 - alpha) * (1 - alpha));
	}
	
	function writeCalcs() {
	  t1 = Date.now();
	  var t = (t1 - t0) / 1000;
	  var para = document.getElementById('codepara');
	  var text;
	  if (t < 1) {
	    text = out.substring(0, Math.floor(out.length * t)) + "_";
	    para.innerHTML = text;
	  }
	  else if (t < 7) {
	    para.innerHTML = out + output.substring(0, Math.floor(output.length * (t - 1) / 6)) + "_";
	
	  }
	  else if (t < 8) {
	    para.innerHTML = out + output + "_";
	  }
	  else if (t < 8.5) {
	    para.innerHTML = out + output;
	  }
	  else if (t < 9) {
	    para.innerHTML = out + output + "_";
	  }
	  else if (t < 9.5) {
	    para.innerHTML = out + output;
	  }
	  else if (t < 11.5) {
	    para.innerHTML = out + output + " " + share.substring(0, Math.round(share.length * smoothAlpha((t - 9.5)/2))) + "_";
	  }
	  else if (t < 12.5) {
	    para.innerHTML = out + output + " " + share + " " + email + "email".substring(0, Math.round(5 * (t - 11.5))) + "</a>" + "_";	
	  }
	  else if (t < 13) {
	    para.innerHTML = out + output + " " + share + " " + email + "email</a> " + "_";
	  }
	  else if (t < 14) {
	    para.innerHTML = out + output + " " + share + " " + email + "email</a> " + twitter + "twitter".substring(0, Math.round(7 * smoothAlpha(t - 13))) + "</a>" + "_";
	  }
	  else if (t < 14.5) {
	    para.innerHTML = out + output + " " + share + " " + email + "email</a> " + twitter + "twitter</a> " + "_";
	  }
	  else if (t < 15.5) {
	    para.innerHTML = out + output + " " + share + " " + email + "email</a> " + twitter + "twitter</a> " + facebook + "facebook".substring(0, Math.round(8 * smoothAlpha(t - 14.5))) + "</a>" + "_";
	  }
	  else if (t < 16) {
	    para.innerHTML = out + output + " " + share + " " + email + "email</a> " + twitter + "twitter</a> " + facebook + "facebook" + "</a>_";
	  }
	  else if (t < 17) {
	    para.innerHTML = out + output + " " + share + " " + email + "email</a> " + twitter + "twitter</a> " + facebook + "facebook</a> " + diaspora + "diaspora".substring(0, Math.round(8 * smoothAlpha(t - 16))) + "</a>" + "_";
	  }	
	  else if (t < 17.5) {
	    
	    para.innerHTML = out + output + " " + share + " " + email + "email</a> " + twitter + "twitter</a> " + facebook + "facebook</a> " + diaspora + "diaspora</a>" + "_";
	  }
     else {
	    para.innerHTML = out + output + " " + share + " " + email + "email</a> " + twitter + "twitter</a> " + facebook + "facebook" + "</a> " + diaspora + "diaspora</a>.";
	    console.log(t);
	    appState = STATE_STILL;
	  }
     var preblock = document.getElementById('preblock');
     window.scrollTo(0, preblock.offsetHeight);
	}
	
	function run() {
	  switch(appState) {
	    case STATE_CALC:
	      //calc not finished
	      writeCalcs();
	      break;
	    case STATE_STILL:
	      //Calcs done
	  }
	}
	
	function gameLoop() {
	  if (appState != STATE_STILL) {
	    window.setTimeout(gameLoop, 20);
	    run();
	  }
	}
	generateOutput();
	gameLoop();
}